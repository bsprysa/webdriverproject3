import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestGmail {

  @Test
  public void gmailTest() {
    System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    WebDriver driver = new ChromeDriver() {
      {
        manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
      }
    };
    driver.get("https://mail.google.com/");

        GmailActions action = new GmailActions(driver);
        action.enterLoginAndSubmit("epam.test.sprysa@gmail.com");
        action.enterPasswordAndSubmit("die34nh2");
        action.openFillAndCloseEmail("sprysa@gmail.com", "Test",
            "Hi from epam.test.sprysa@gmail.com");
        action.openDraftAndSendEmail();
//        driver.quit();
  }
}
