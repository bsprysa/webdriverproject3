import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailActions {

  @FindBy(xpath = "//input[@id='identifierId']")
  private WebElement inputLogin;

  @FindBy(id = "identifierNext")
  private WebElement buttonSendEmailAddress;

  @FindBy(css = "input[type='password']:nth-child(1)")
  private WebElement inputPassword;

  @FindBy(id = "passwordNext")
  private WebElement buttonSendPassword;

  @FindBy(xpath = "//div[@class='z0']//div[@role='button']")
  private WebElement buttonCompose;

  @FindBy(className = "vO")
  private WebElement inputAddress;

  @FindBy(className = "aoT")
  private WebElement inputSubject;

  @FindBy(xpath = "//div[@class='Ar Au']//div[@role='textbox']")
  private WebElement inputMessage;

  @FindBy(className = "Ha")
  private WebElement closeEmail;

  @FindBy(xpath = "//div[@class='TN bzz aHS-bnq']")
  private WebElement draftOpen;

  @FindBy(id = ":a0")
  private WebElement openDraftEmail;

  @FindBy(xpath = "//div[@class='J-J5-Ji btA']//div[@role='button']")
  private WebElement buttonSendEmail;

  WebDriver driver;

  public GmailActions(WebDriver driver) {
    PageFactory.initElements(driver, this);
    this.driver = driver;
  }

  public void enterLoginAndSubmit(String email) {
    inputLogin.sendKeys(email);
    buttonSendEmailAddress.click();
  }

  public void enterPasswordAndSubmit(String password) {
    inputPassword.sendKeys(password);
    buttonSendPassword.click();
  }

  public void openFillAndCloseEmail(String address, String subject, String message) {
    buttonCompose.click();
    inputAddress.sendKeys(address);
    inputSubject.sendKeys(subject);
    inputMessage.sendKeys(message);
    new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(closeEmail)).click();
//        closeEmail.click();
  }

  public void openDraftAndSendEmail() {
    new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(draftOpen)).click();
//        draftOpen.click();
//        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(openDraftEmail)).click();
    openDraftEmail.click();
//        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(buttonSendEmail)).click();
    buttonSendEmail.click();
  }


}
